from pydantic import BaseModel


class City(BaseModel):
    country: str
    name: str
    lat: float
    lng: float


class Result(BaseModel):
    experiment_name: str
    set_time: float
    get_time: float
