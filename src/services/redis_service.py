from typing import Optional

import redis


class Redis:
    def __init__(self, config):
        self.config = config
        self.redis: Optional[redis.Redis] = None

        self.connect()

    def connect(self):
        self.redis = redis.Redis(
            host=self.config["host"],
            port=self.config["port"],
            db=self.config["db"]
        )

    def close_connection(self):
        self.redis.close()

    def ping(self):
        return self.redis.ping()

    def get(self, key):
        return self.redis.get(key)

    def set(self, key, value):
        self.redis.set(key, value)

    def set_hset(self, name, key, value):
        self.redis.hset(name, key, value)

    def get_hget(self, name, key):
        return self.redis.hget(name, key)

    def set_zadd(self, name, mapping):
        return self.redis.zadd(name, mapping)

    def get_zget(self, name, data_size):
        return self.redis.zrange(name, 0, data_size)

    def set_list(self, name, values):
        return self.redis.lpush(name, values)

    def ltrim(self, name, start, end):
        return self.redis.ltrim(name, start, end)

    def get_list(self, name, _index):
        return self.redis.lindex(name, _index)

    def get_list_all(self, name):
        return self.redis.lrange(name, 0, -1)