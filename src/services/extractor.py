import json

from typing import List

from models import City


class Extractor:
    def __init__(self, path):
        self.path = path

    def extract(self) -> List[City]:
        city_list = []
        with open(self.path) as f:
            cities = json.load(f)
            for city in cities:
                city_model = City.parse_obj(city)
                city_list.append(city_model)
        return city_list
