from services import Redis

if __name__ == '__main__':
    redis_config = {
        "host": "localhost",
        "port": 6379,
        "db": 0
    }
    redis = Redis(redis_config)
    redis_ping_answer = redis.ping()
    if redis_ping_answer:
        print("Redis is running")
    else:
        print("Redis is not running")

    incoming_articles = [
        {
            "title": "Some title",
            "text": "Some text",
        }, {
            "title": "Some title",
            "text": "Some text",
        },
    ]

    redis.set_list("articles", incoming_articles)
    redis.ltrim("articles", 0, 1)

    all_articles = redis.get_list_all("articles")
    for index, article in enumerate(all_articles):
        redis.set_zadd("articles", {"index": index, "article": article})