import time

from models import Result
from services import Extractor, Redis

if __name__ == '__main__':
    cities = Extractor('data/cities.json').extract()
    redis_config = {
        "host": "localhost",
        "port": 6379,
        "db": 0
    }

    redis = Redis(redis_config)
    redis_ping_answer = redis.ping()
    if redis_ping_answer:
        print("Redis is running")
    else:
        print("Redis is not running")

    # STRING
    set_start_time = time.time()
    for city in cities:
        redis.set(city.name, city.json())
    set_end_time = time.time()

    get_start_time = time.time()
    for city in cities:
        redis.get(city.name)
    get_end_time = time.time()
    string_result = Result(experiment_name="STRING", set_time=set_end_time - set_start_time,
                           get_time=get_end_time - get_start_time)

    # HSET
    set_start_time = time.time()
    for city in cities:
        redis.set_hset("cities", city.name, city.json())
    set_end_time = time.time()

    get_start_time = time.time()
    for city in cities:
        redis.get_hget("cities", city.name)
    get_end_time = time.time()
    hset_result = Result(experiment_name="HSET", set_time=set_end_time - set_start_time,
                         get_time=get_end_time - get_start_time)

    # ZSET
    set_start_time = time.time()
    for city in cities:
        redis.set_zadd("cities", {"is_city": True, "Data": city.json()})
    set_end_time = time.time()

    get_start_time = time.time()
    redis.get_zget("cities", len(cities))
    get_end_time = time.time()
    zset_result = Result(experiment_name="ZSET", set_time=set_end_time - set_start_time,
                         get_time=get_end_time - get_start_time)

    # LIST
    cities_json = [city.json() for city in cities]
    set_start_time = time.time()
    redis.set_list("cities", cities_json)
    set_end_time = time.time()

    get_start_time = time.time()
    redis.get_list("cities", 0)
    get_end_time = time.time()
    list_result = Result(experiment_name="LIST", set_time=set_end_time - set_start_time,
                         get_time=get_end_time - get_start_time)

    print(string_result.json())
    print(hset_result.json())
    print(zset_result.json())
    print(list_result.json())

    redis.close_connection()
